import mongoose from 'mongoose'

const connectToDatabase = async () => {
    try {
        const conn = await mongoose.connect(process.env.MONGODB_URI)
        console.log(`MongoDB Connected: ${conn.connection.host}`)
    }
    catch (error) {
        console.error(`Error: ${error.message}`)
        process.exit(1)
    }
}

export default connectToDatabase

