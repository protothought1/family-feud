import express from 'express'
import dotenv from 'dotenv'
import connectToDatabase from './config/mongodb.js'

dotenv.config()
connectToDatabase()

const app = express()

app.get('/', (req, res) => res.send('Server is ready!'))

const port = process.env.PORT || 5000
app.listen(port, () => console.log(`Server started on port ${port}.`))
