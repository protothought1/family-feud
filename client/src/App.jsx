import Board from './Components/Board/Board'
import Teams from './Components/Teams/Teams'
import './App.css'

function App() {
  return (
    <>
      <Board />
      <Teams />
    </>
  )
}

export default App
