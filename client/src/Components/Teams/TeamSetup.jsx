import { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { updateTeamInfo } from '../../features/teams/teamsSlice'
import { Button, TextField } from '@mui/material'
import './TeamSetup.css'

function TeamSetup() {

  const maxPlayers = 5

  const team1NameDefault = useSelector((state) => state.teams.team1.defaultName)
  const team2NameDefault = useSelector((state) => state.teams.team2.defaultName)
  const savedTeam1Name = useSelector((state) => state.teams.team1.name)
  const savedTeam2Name = useSelector((state) => state.teams.team2.name)
  const lockedInPlayersTeam1 = useSelector((state) => state.teams.team1.players)
  const lockedInPlayersTeam2 = useSelector((state) => state.teams.team2.players)

  const formatPlayerInput = (teamNumber) => {
    let lockedInPlayersCopy = []
    if (teamNumber == 1) {
      lockedInPlayersCopy = lockedInPlayersTeam1
      const diff = (maxPlayers+1) - lockedInPlayersTeam1.length
      const filler = Array(diff).fill('')
      lockedInPlayersCopy = lockedInPlayersCopy.concat(filler)
    }
    else {
      lockedInPlayersCopy = lockedInPlayersTeam2
      const diff = (maxPlayers+1) - lockedInPlayersTeam2.length
      const filler = Array(diff).fill('')
      lockedInPlayersCopy = lockedInPlayersCopy.concat(filler)
    }
    return lockedInPlayersCopy
  }
  
  const team1PlayersFormatted = formatPlayerInput(1)
  const team2PlayersFormatted = formatPlayerInput(2)

  const FFGradientLight = 'linear-gradient(180deg, rgba(255,255,255,1) 0%, rgba(137,205,242,1) 100%)'
  // const FFGradientDark = 'linear-gradient(to bottom, #7db9e8 0%, #207cca 49%, #2989d8 50%, #1e5799 100%)'
  const FFGradientDark = 'linear-gradient(to bottom, #7db9e8 0%, #2989d8 50%, #1e5799 100%)'


  const [team1Name, setTeam1Name] = useState(savedTeam1Name)
  const [team2Name, setTeam2Name] = useState(savedTeam2Name)
  const [priorTeam1Name, setPriorTeam1Name] = useState(team1NameDefault)
  const [priorTeam2Name, setPriorTeam2Name] = useState(team2NameDefault)
  const [team1Players, setTeam1Players] = useState(team1PlayersFormatted)
  const [team2Players, setTeam2Players] = useState(team2PlayersFormatted)

  const dispatch = useDispatch()

  const handleTeamNameFocus = (teamNumber) => {
    if (teamNumber == 1 && team1Name == team1NameDefault) {
      setPriorTeam1Name(team1Name)
      setTeam1Name('')
    }
    if (teamNumber == 2 && team2Name == team2NameDefault) {
      setPriorTeam2Name(team2Name)
      setTeam2Name('')
    }
  }

  const handleTeamNameBlur = (teamNumber) => {
    if (teamNumber == 1 && team1Name == '') setTeam1Name(priorTeam1Name)
    if (teamNumber == 2 && team2Name == '') setTeam2Name(priorTeam2Name)
  }

  const updateTeamName = (teamNumber, name) => {
    teamNumber == 1 ? setTeam1Name(name.toUpperCase()) : setTeam2Name(name.toUpperCase())
  }

  const updatePlayerName = (info) => {
    const { teamNumber, playerIndex, name } = info
    if (teamNumber == 1) {
      const updatedPlayers = team1Players.map((p, i) => {
        if (i === playerIndex) { return name }
        else { return p }
      })
      setTeam1Players(updatedPlayers)
    }
    else {
      const updatedPlayers = team2Players.map((p, i) => {
        if (i === playerIndex) { return name }
        else { return p }
      })
      setTeam2Players(updatedPlayers)
    }
  }

  const handleTeamLockIn = () => {
    const team1Filtered = team1Players.filter(p => p)
    const team2Filtered = team2Players.filter(p => p)
    const teamInfo = { team1Name, team1Players: team1Filtered, team2Name, team2Players: team2Filtered }
    dispatch(updateTeamInfo(teamInfo))
  }

  const requiredFieldEmpty = (info) => {
    const { teamNumber, playerIndex } = info
    if (teamNumber == 1) {
      return !team1Players[playerIndex] && team1Players[playerIndex+1].length > 0
    }
    else {
      return !team2Players[playerIndex] && team2Players[playerIndex+1].length > 0
    }
  }

  const canAddPlayer = (info) => {
    const { teamNumber, playerIndex } = info
    let numPlayerNames = 0
    for (let i = playerIndex; i >= 0; i--) { // Start from player index and check prior player inputs
      if (teamNumber == 1 && team1Players[i].length > 0) numPlayerNames += 1
      if (teamNumber == 2 && team2Players[i].length > 0) numPlayerNames += 1
    }
    return (numPlayerNames < playerIndex)
  }

  const playerFieldsHaveError = () => {
    if (team1Players[0].length < 1 || team2Players[0].length < 1) return true
    for (let i = 1; i < maxPlayers; i++) {
      return (team1Players[i].length > 0) && (team1Players[i-1] < 1)
    }
  }

  const styles = {
    teamName: {
      backgroundSize: "100%",
      backgroundcolor: "primary",
      backgroundImage: FFGradientLight,
      backgroundClip: 'text',
      WebkitBackgroundClip: "text",
      WebkitTextFillColor: "transparent",
      fontSize: '25px',
      textAlign: 'center',
    },
    playerName: {
      color: `rgb(255, 255, 255, 0.9)`,
      fontSize: '20px',
      textAlign: 'center',
    }
  }

  return (
  <div className='teamSetupContainer'>
    <div className='teamsSetup'>
      <div className='teamSetup'>
        <div className='teamSetupName'>
          <TextField 
            variant="outlined"
            value={team1Name} 
            inputProps={{style: styles.teamName}}
            onFocus={() => handleTeamNameFocus(1)} 
            onBlur={() => handleTeamNameBlur(1)}
            onChange={(e)=>updateTeamName(1,e.target.value)}
          />
        </div>
        <div className='playersSetup'>
          <TextField 
            variant="standard" placeholder='Add Player'
            error={requiredFieldEmpty({teamNumber:1, playerIndex:0})}
            value={team1Players[0]}
            inputProps={{style: styles.playerName}} 
            onChange={(e)=>updatePlayerName({teamNumber:1, playerIndex:0, name: e.target.value})}
          />
          <TextField 
            variant="standard" placeholder='Add Player'
            value={team1Players[1]}
            error={requiredFieldEmpty({teamNumber:1, playerIndex:1})}
            disabled={canAddPlayer({teamNumber:1, playerIndex:1})}
            inputProps={{style: styles.playerName}} 
            onChange={(e)=>updatePlayerName({teamNumber:1, playerIndex:1, name: e.target.value})}
          />
          <TextField 
            variant="standard" placeholder='Add Player'
            value={team1Players[2]}
            error={requiredFieldEmpty({teamNumber:1, playerIndex:2})}
            disabled={canAddPlayer({teamNumber:1, playerIndex:2})}
            inputProps={{style: styles.playerName}} 
            onChange={(e)=>updatePlayerName({teamNumber:1, playerIndex:2, name: e.target.value})}
          />
          <TextField 
            variant="standard" placeholder='Add Player'
            value={team1Players[3]}
            error={requiredFieldEmpty({teamNumber:1, playerIndex:3})}
            disabled={canAddPlayer({teamNumber:1, playerIndex:3})}
            inputProps={{style: styles.playerName}} 
            onChange={(e)=>updatePlayerName({teamNumber:1, playerIndex:3, name: e.target.value})}
          />
          <TextField 
            variant="standard" placeholder='Add Player'
            value={team1Players[4]}
            error={requiredFieldEmpty({teamNumber:1, playerIndex:4})}
            disabled={canAddPlayer({teamNumber:1, playerIndex:4})}
            inputProps={{style: styles.playerName}} 
            onChange={(e)=>updatePlayerName({teamNumber:1, playerIndex:4, name: e.target.value})}
          />
        </div>
      </div>
      <div className='teamSetup'>
        <div className='teamSetupName'>
          <TextField 
            variant="outlined"
            value={team2Name} 
            inputProps={{style: styles.teamName}}
            onFocus={() => handleTeamNameFocus(2)} 
            onBlur={() => handleTeamNameBlur(2)}
            onChange={(e)=>updateTeamName(2,e.target.value)}
          />
        </div>
        <div className='playersSetup'>
            <TextField 
              variant="standard" placeholder='Add Player'
              error={requiredFieldEmpty({teamNumber:2, playerIndex:0})}
              value={team2Players[0]}
              inputProps={{style: styles.playerName}} 
              onChange={(e)=>updatePlayerName({teamNumber:2, playerIndex:0, name: e.target.value})}
            />
            <TextField 
              variant="standard" placeholder='Add Player'
              value={team2Players[1]}
              error={requiredFieldEmpty({teamNumber:2, playerIndex:1})}
              disabled={canAddPlayer({teamNumber:2, playerIndex:1})}
              inputProps={{style: styles.playerName}} 
              onChange={(e)=>updatePlayerName({teamNumber:2, playerIndex:1, name: e.target.value})}
            />
            <TextField 
              variant="standard" placeholder='Add Player'
              value={team2Players[2]}
              error={requiredFieldEmpty({teamNumber:2, playerIndex:2})}
              disabled={canAddPlayer({teamNumber:2, playerIndex:2})}
              inputProps={{style: styles.playerName}} 
              onChange={(e)=>updatePlayerName({teamNumber:2, playerIndex:2, name: e.target.value})}
            />
            <TextField 
              variant="standard" placeholder='Add Player'
              value={team2Players[3]}
              error={requiredFieldEmpty({teamNumber:2, playerIndex:3})}
              disabled={canAddPlayer({teamNumber:2, playerIndex:3})}
              inputProps={{style: styles.playerName}} 
              onChange={(e)=>updatePlayerName({teamNumber:2, playerIndex:3, name: e.target.value})}
            />
            <TextField 
              variant="standard" placeholder='Add Player'
              value={team2Players[4]}
              error={requiredFieldEmpty({teamNumber:2, playerIndex:4})}
              disabled={canAddPlayer({teamNumber:2, playerIndex:4})}
              inputProps={{style: styles.playerName}} 
              onChange={(e)=>updatePlayerName({teamNumber:2, playerIndex:4, name: e.target.value})}
            />
        </div>
      </div>
    </div>
    <div>
      <Button 
        variant='contained'
        disabled={playerFieldsHaveError()}
        sx={{
          fontWeight: 700,
          border: '2px solid white',
          backgroundColor: '#7db9e8',
          background: FFGradientDark
        }}
        onClick={handleTeamLockIn}
      >
        Lock In Teams
      </Button>
    </div>
  </div>
  )
}

export default TeamSetup
