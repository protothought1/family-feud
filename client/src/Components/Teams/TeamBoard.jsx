import { useSelector, useDispatch } from 'react-redux'
import { updateTeamLockIn } from '../../features/teams/teamsSlice'
import { Button } from '@mui/material'

import './TeamBoard.css'

function TeamBoard() {
  const FFGradientDark = 'linear-gradient(to bottom, #7db9e8 0%, #2989d8 50%, #1e5799 100%)'
  const dispatch = useDispatch()

  const team1Name = useSelector((state) => state.teams.team1.name)
  const team2Name = useSelector((state) => state.teams.team2.name)
  const team1Players = useSelector((state) => state.teams.team1.players)
  const team2Players = useSelector((state) => state.teams.team2.players)

  const unlockTeams = () => {
    dispatch(updateTeamLockIn({teamsLockedIn: false}))
  }

  return (
    <div className='teamBoardContainer'>
      <div className='teams'>
        <div className='team'>
          <div className='teamName teamNameInactive'>{team1Name}</div>
          <div className='players'>
              {team1Players && team1Players
                .map((player, idx) => {
                  return (<div key={idx} className='player'>{player}</div>)
                })}
          </div>
        </div>
        <div className='team'>
          <div className='teamName teamNameInactive'>{team2Name}</div>
          <div className='players'>
              {team2Players && team2Players
                .map((player, idx) => {
                  return (<div key={idx} className='player'>{player}</div>)
                })}
          </div>
        </div>
      </div>
      <div>
        <Button 
          variant='contained'
          sx={{
            fontWeight: 700,
            border: '2px solid white',
            backgroundColor: '#7db9e8',
            background: FFGradientDark
          }}
          onClick={unlockTeams}
        >
          Edit Teams
        </Button>
      </div>
    </div>
  )
}

export default TeamBoard
