import { useSelector, useDispatch } from 'react-redux'
import TeamBoard from './TeamBoard'
import TeamSetup from './TeamSetup'
import './Teams.css'

function Teams() {

  const teamInfo = useSelector((state) => state.teams)
  const teamsLockedIn = useSelector((state) => state.teams.lockedIn)
  const display = teamsLockedIn ? <TeamBoard /> : <TeamSetup />
  return (
    <>
      {display}
    </>
  )
}

export default Teams
