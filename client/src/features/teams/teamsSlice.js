import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    team1: {
        defaultName: 'TEAM 1',
        name: 'TEAM 1',
        players: ['','','','','',''],
        score: 0
    },
    team2: {
        defaultName: 'TEAM 2',
        name: 'TEAM 2',
        players: ['','','','','',''],
        score: 0
    },
    lockedIn: false
}

export const teamsSlice = createSlice({
    name: 'teams',
    initialState,
    reducers: {
        updateTeamInfo: (state, action) => {
            const { team1Name, team1Players, team2Name, team2Players } = action.payload
            state.team1.name = team1Name
            state.team1.players = team1Players
            state.team2.name = team2Name
            state.team2.players = team2Players
            state.lockedIn = true
        },
        updateTeamName: (state, action) => {
            const { teamNumber, teamName } = action.payload
            if (teamNumber == 1) state.team1.name = teamName
            else state.team2.name = teamName
        },
        updatePlayers: (state, action) => {
            const { teamNumber, players } = action.payload
            if (teamNumber == 1) state.team1.players = players
            else state.team2.players = players
        },
        updateScore: (state, action) => {
            const { teamNumber, runningTotal } = action.payload
            if (teamNumber == 1) state.team1.score += runningTotal
            else state.team2.score += runningTotal
        },
        updateTeamLockIn: (state, action) => {
            state.lockedIn = action.payload.teamsLockedIn
        }
    }
})

export const { updateTeamInfo, updateTeamName, updatePlayers, updateScore, updateTeamLockIn } = teamsSlice.actions
export default teamsSlice.reducer